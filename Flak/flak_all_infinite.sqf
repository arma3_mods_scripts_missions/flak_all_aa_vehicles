// This script is for maintaining infinite flak for any AA vehicles.

// Using the below commented out command, set the total ammo for the autocannon in the ZSU to 680, so that this script can work. 680 is taken since that is the total ammo count for the Tigris. The flak ammo is infinite, so it won't make any difference, if set to 680.
// this setAmmo [weapons this select 0, 680];
// this addEventHandler ["Fired", {deleteVehicle (_this select 6); [_this, 680, 678, 5000, 10, 5, 5, 300, "SmallSecondary", 500, 1000, 2] execVM "Flak\flak_all_infinite.sqf";}];

// START Param variables to enable modularity

// Total count of ammunition present in Flak AA vehicle.
_totalAmmoParam = param [1, 680, [1]];

// When Flak vehicle has less than this ammo count (total ammo 680) it will spawn flak.  IE value 678 will spawn one flak per three shots.
_ammoCountForFlakParam = param [2, 678, [1]];
//When Flak vehicle has less than this ammo count (total ammo 2000) it will spawn flak.  IE value 1978 will spawn one flak per three shots.
_maxDistanceParam = param [3, 8000, [1]];

//Linear Dispersion when speed is zero, dispersion added is zero.  When speed is 100 km/h, dispersion will equal this variable
_speedDispersionParam = param [4, 10, [1]];

//Linear Dispersion when distance is zero, dispersion added is zero.  When distance is 500m, dispersion will equal this variable
_distanceDispersionParam = param [5, 5, [1]];

//Height target must be for flak to activate (IE target can fly "under the radar")
_targetMinHeightParam = param [6, 20, [1]];

// Maximum height of flak blanket
_flakMaxHeightParam = param [7, 500, [1]];

// Type of explosion. Includes : SmallSecondary, HelicopterExploBig
_setTypeExploParam = param [8, "SmallSecondary", [""]];

// Scales the dispersion of flak spawned depending upon the liner speed of the target. Something like MinMax Scaling/ Standard Scaling.
_speedDispersionScalerParam = param [9, 300, [1]];

// Scales the dispersion of flak spawned depending upon the liner distance of the target from the flak AA gun. Something like MinMax Scaling/ Standard Scaling.
_distanceDispersionScalerParam = param [10, 1000, [1]];

// Constant for randomising flak spawn location.
_flakSpawnRandomParam = param [11, 1.5, [1]];

// END Param variables

_unit = _this select 0 select 0;
_weapon = _this select 0 select 2;
// _setTypeExplo = _this select 1;

if (_unit ammo _weapon < _ammoCountForFlakParam) 
    then 
    {
        (_unit) setAmmo [_weapon, _totalAmmoParam];
        _tarPos = [];
        _target = 0;
        if (isPlayer (assignedGunner _unit)) 
            then 
            {
                _target = cursorTarget;
                if (_unit distance _target < _maxDistance) 
                    then 
                    {
                        _tarPos = getPos _target;
                    };
            } 
        else 
        {
            _possTar = _unit nearTargets _maxDistanceParam;
            if ((count _possTar) > 0) 
                then 
                {
                    _i = 0;
                    _hold = 0;
                    {   
                        _i = _unit aimedAtTarget [_x select 4,_weapon];
                        if (_i > _hold && (_x select 3) > 0) then {
                            _target = _x select 4;
                            _tarPos = _x select 0;
                            _hold = _i;
                        };
                    } forEach _possTar;
                };
        };
        
        if ((count _tarPos) > 0) 
            then 
            {
                _tarX = _tarPos select 0;
                _tarY = _tarPos select 1;
                _tarZ = _tarPos select 2;
                if (_tarZ > _targetMinHeightParam) 
                    then 
                    {
                        if !(lineIntersects [getPos _unit, _tarPos, _unit, _target]) then {
                            _flakDis = ((speed _target) * (_speedDispersionParam / _speedDispersionScalerParam)) + ((_unit distance _target) * (_distanceDispersionParam / _distanceDispersionScalerParam));
                            _disX = ((random (_flakDis * _flakSpawnRandomParam)) - _flakDis) + _tarX;
                            _disY = ((random (_flakDis * _flakSpawnRandomParam)) - _flakDis) + _tarY;
                           
                            if (_tarZ > _flakMaxHeightParam) 
                                then 
                                {
                                  _tarZ = _flakMaxHeightParam;
                                };

                            _disZ = ((random (_flakDis * _flakSpawnRandomParam)) - _flakDis) + _tarZ;
                            _flak = createVehicle [_setTypeExploParam, [_disX, _disY, _disZ], [], 0, "CAN_COLLIDE"];
                        };
                    };
            };
    };