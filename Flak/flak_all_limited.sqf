// Flak is limited to the total ammunition count of the AA vehicle. There is a bug on line 27, do not use this currently.

// Place this line in the init of any AA vehicle to make it spawn limited flak. Remember to remove the missiles if present.
// this addEventHandler ["Fired", {deleteVehicle (_this select 6); _this execVM "Flak\flak_all_infinite.sqf";}];

// START Param variables to enable modularity

// When Flak vehicle has less than this ammo count (total ammo 680) it will spawn flak.  IE value 678 will spawn one flak per three shots.
_ammoCountForFlakParam = param [1, 3, [1]];
//When Flak vehicle has less than this ammo count (total ammo 2000) it will spawn flak.  IE value 1978 will spawn one flak per three shots.
_maxDistanceParam = param [2, 8000, [1]];

//Linear Dispersion when speed is zero, dispersion added is zero.  When speed is 100 km/h, dispersion will equal this variable
_speedDispersionParam = param [3, 10, [1]];

//Linear Dispersion when distance is zero, dispersion added is zero.  When distance is 500m, dispersion will equal this variable
_distanceDispersionParam = param [4, 5, [1]];

//Height target must be for flak to activate (IE target can fly "under the radar")
_targetMinHeightParam = param [5, 20, [1]];

// Maximum height of flak blanket
_flakMaxHeightParam = param [6, 500, [1]];

// Type of explosion. Includes : SmallSecondary, HelicopterExploBig
_setTypeExploParam = param [7, "SmallSecondary", [""]];

// Scales the dispersion of flak spawned depending upon the liner speed of the target. Something like MinMax Scaling/ Standard Scaling.
_speedDispersionScalerParam = param [8, 300, [1]];

// Scales the dispersion of flak spawned depending upon the liner distance of the target from the flak AA gun. Something like MinMax Scaling/ Standard Scaling.
_distanceDispersionScalerParam = param [9, 1000, [1]];

// Constant for randomising flak spawn location.
_flakSpawnRandomParam = param [10, 1.5, [1]];

// END Param variables

//END Editable variables

_unit = _this select 0 select 0;
_weapon = _this select 0 select 2;
_ammoPresent = _unit ammo _weapon;
_ammoRemainder = _ammoPresent % _ammoCountForFlak;

if (_ammoRemainder == 0) 
    then 
    {
        // (_unit) setAmmo [_weapon, _totalAmmo];
        _tarPos = [];
        _target = 0;
        if (isPlayer (assignedGunner _unit)) 
            then 
            {
                _target = cursorTarget;
                if (_unit distance _target < _maxDistance) 
                    then 
                    {
                        _tarPos = getPos _target;
                    };
            } 
        else 
        {
            _possTar = _unit nearTargets _maxDistance;
            if ((count _possTar) > 0) 
                then 
                {
                    _i = 0;
                    _hold = 0;
                    {   
                        _i = _unit aimedAtTarget [_x select 4,_weapon];
                        if (_i > _hold && (_x select 3) > 0) then {
                            _target = _x select 4;
                            _tarPos = _x select 0;
                            _hold = _i;
                        };
                    } forEach _possTar;
                };
        };
        
        if ((count _tarPos) > 0) 
            then 
            {
                _tarX = _tarPos select 0;
                _tarY = _tarPos select 1;
                _tarZ = _tarPos select 2;
                if (_tarZ > _targetMinHeight) 
                    then 
                    {
                        if !(lineIntersects [getPos _unit, _tarPos, _unit, _target]) then {
                            _flakDis = ((speed _target) * (_speedDispersionParam / _speedDispersionScalerParam)) + ((_unit distance _target) * (_distanceDispersionParam / _distanceDispersionScalerParam));
                            _disX = ((random (_flakDis * _flakSpawnRandomParam)) - _flakDis) + _tarX;
                            _disY = ((random (_flakDis * _flakSpawnRandomParam)) - _flakDis) + _tarY;

                            if (_tarZ > _flakMaxHeightParam)
                                then
                                {
                                    _tarZ = _flakMaxHeightParam;
                                }

                            _disZ = ((random (_flakDis * _flakSpawnRandomParam)) - _flakDis) + _tarZ;

                            _flak = createVehicle [_setTypeExploParam,[_disX, _disY, _disZ],[],0,"CAN_COLLIDE"];
                        };
                    };
            };
    };